# Evaluation Criteria

Below are all the criteria we use to evaluate each option in the [Score Card](ddd).

The GitHub Evacuation Guide will not provide “the one true answer”. It will have an assessment of the various options, on various dimensions such as **principles** (privacy, no user or data lock-in, independence, distributed, etc), **pragmatism** (works today vs achieves all goals perfectly) and of course **features** (source code mgmt, issues, forums, etc). The principles are opinionated, but the rest will be an honest, fair assessment against those principles and the other dimensions. People will decide for themselves.



> ### TODO
>
> This was an off-the-cuff first cut. I (@vassudanagunta) am going to refine and clean up this list, and define each one. But don't hesitate to add to the list or start defining the items if this excites you!



## principles / values

- Users First?
  - privacy
  - data freedom, users own their data, no user or data lock-in, easy to export/migrate out *in its entirety*.
- anti-concentration of power
  - independence
  - distributed (is this a principle, and ends, or really just a means to a different principle, e.g. independence?)
- no advertising
- commitment to social good
- users are the one and only customer. no ambiguity about customer. it's clear who the customer is. the customer is the source of all revenue If there is revenue).
- who's paying? (users/advertisers/freemium)



## features

- source code mgmt
- issues
- pull requests
  - via web
- forums
- community
- hosted (vs self-hosted)



## pragmatism

- difficult/easy for owners
- difficult/easy for contributors
- difficult/easy for followers
- accessible (not just to hardcore techies), difficulty level 1-5, go being 1 or 2. 
- works today
- if not, when? (in min years, likely years) what's the roadmap?
- track record?
- number of existing users / size of community
- achieves all goals perfectly
- will your users/contributors migrate with you? will they have to create a new account / one more account to manage? (e.g. GitLab accepts GitHub login. We don't want to encourage dependence on GitHub, but getting off GitHub and only using their login is better than keeping everything on GitHub. Sometimes revolutions are evolutions. 

