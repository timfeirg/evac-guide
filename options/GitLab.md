# option: GitLab



## Evaluation



GitLab supports "[open source push mirroring](https://about.gitlab.com/2018/05/22/gitlab-10-8-released/)"







## Migrating to GitLab

There is a [YouTube video](https://www.youtube.com/watch?v=VYOXuOg9tQI) posted by the GitLab staff that explains the process nicely. This will probably take a long time to complete, so you should run it overnight. Once you start the process, you can close the browser tab containing your repositories and it will continue to import them.